package com.home.service;

import com.home.model.FileExt;
import com.home.model.QueueType;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class MainParserTest {

    @Autowired
    MainParser mainParser;

    @Test
    void parseFiles() {
        mainParser.setArgs(new String[]{"src/test/resources/files/orders1.csv", "src/test/resources/files/orders2.json"});
        mainParser.parseFiles();
        while (MainParser.threadQueue.isContinueWork()) {
        }
        Assert.assertEquals(16, MainParser.threadQueue.size());
    }

    @Test
    void parseEmptyFiles() {
        mainParser.setArgs(new String[]{"src/test/resources/files/orders3.csv"});
        mainParser.parseFiles();
        while (MainParser.threadQueue.isContinueWork()) {
        }
        Assert.assertEquals(0, MainParser.threadQueue.size());
    }

    @Test
    void parseWrongFiles() {
        mainParser.setArgs(new String[]{"src/test/resources/files/notfound.csv"});
        mainParser.parseFiles();
        while (MainParser.threadQueue.isContinueWork()) {
        }
        Assert.assertEquals(0, MainParser.threadQueue.size());
    }

    @SneakyThrows
    @Test
    void convertData() {
        MainParser.threadQueue.put(new QueueType(FileExt.CSV, "1,100,USD,оплата заказа", 1, "src/test/resources/files/orders1.csv"));
        MainParser.threadQueue.put(new QueueType(FileExt.CSV, "2,,EUR,оплата заказа", 2, "src/test/resources/files/orders1.csv"));
        MainParser.threadQueue.put(new QueueType(FileExt.CSV, "3,100,USD,", 3, "src/test/resources/files/orders1.csv"));
        MainParser.threadQueue.put(new QueueType(FileExt.CSV, "\"asd\",\"sad\",1,13213 123123", 4, "src/test/resources/files/orders1.csv"));
        mainParser.convertData();
        while (!MainParser.threadQueue.isEmpty()){
        }
        Assert.assertEquals(0, MainParser.threadQueue.size());
    }
}