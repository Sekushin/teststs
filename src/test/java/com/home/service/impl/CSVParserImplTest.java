package com.home.service.impl;

import com.home.model.FileExt;
import com.home.model.QueueType;
import com.home.service.MainParser;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import javax.validation.Validation;
import javax.validation.Validator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class CSVParserImplTest {
    @Autowired
    CSVParserImpl csvParser;


    private final ByteArrayOutputStream out = new ByteArrayOutputStream();
    private final ByteArrayOutputStream err = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @Before
    public void setStreams() {
        System.setOut(new PrintStream(out));
        System.setErr(new PrintStream(err));
    }

    @After
    public void restoreInitialStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }
    @Test
    void convertFromQueue() {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        QueueType queueType = new QueueType(FileExt.CSV, "1,100,USD,оплата заказа", 1, "src/test/resources/files/orders1.csv");
//        MainParser.threadQueue.put(new QueueType(FileExt.CSV, "2,,EUR,оплата заказа", 2, "src/test/resources/files/orders1.csv"));
//        MainParser.threadQueue.put(new QueueType(FileExt.CSV, "3,100,USD,", 3, "src/test/resources/files/orders1.csv"));
//        MainParser.threadQueue.put(new QueueType(FileExt.CSV, "\"asd\",\"sad\",1,13213 123123", 4, "src/test/resources/files/orders1.csv"));
        csvParser.convertFromQueue(queueType, validator);

        ByteArrayOutputStream outSpy = new ByteArrayOutputStream();
        ConsoleWriter writer = new ConsoleWriter(new PrintStream(outSpy));
        writer.printSomething();
        Assert.assertEquals(outSpy.toString(), is("expected output"));

        Assert.assertEquals("hello", out.toString());
    }
}