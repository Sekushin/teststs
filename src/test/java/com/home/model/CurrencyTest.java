package com.home.model;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class CurrencyTest {

    @Test
    void valueOfByType() {
        Assert.assertEquals(Currency.EUR, Currency.valueOfByType("eur"));
        Assert.assertNull(Currency.valueOfByType(""));
        Assert.assertNull(Currency.valueOfByType(null));
        Assert.assertNull(Currency.valueOfByType("eur1"));
    }
}