package com.home.model;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class FileExtTest {

    @Test
    void valueOfType() {
        Assert.assertEquals(FileExt.CSV, FileExt.valueOfByType("csv"));
        Assert.assertNull(FileExt.valueOfByType("1"));
        Assert.assertNull(FileExt.valueOfByType(null));
        Assert.assertNull(Currency.valueOfByType("docx"));
    }
}