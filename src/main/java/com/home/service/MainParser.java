package com.home.service;

import com.home.model.FileExt;
import com.home.model.Order;
import com.home.model.QueueType;
import com.home.model.ThreadSafeQueue;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import javax.validation.Validation;
import javax.validation.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

@Slf4j
public class MainParser {

    private static final int MAX_COUNT_PARSER_THREAD = 5;
    private static final int MAX_COUNT_CONVERTER_THREAD = 10;
    private static final int CAPACITY = 2000;
    private static final int TIME_OUT = 10;
    public static final ThreadSafeQueue<QueueType> threadQueue = new ThreadSafeQueue<>(CAPACITY, TIME_OUT, TimeUnit.SECONDS);
    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    private final Map<FileExt, FileParser> parsers;
    private String[] args;

    public MainParser(Map<FileExt, FileParser> parsers) {
        this.parsers = parsers;
    }

    public void setArgs(String[] sourceArgs) {
        this.args = sourceArgs;
    }

    public void parseFiles() {
        try {
            ThreadPoolExecutor parserExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(Math.min(args.length, MAX_COUNT_PARSER_THREAD));
            CountDownLatch countDownLatch = new CountDownLatch(this.args.length);
            for (String file : this.args) {
                if (file != null && !file.isEmpty()) {
                    parserExecutor.submit(() -> {
                        try (BufferedReader br = Files.newBufferedReader(Paths.get(file))) {
                            String line;
                            FileExt fileType = FileExt.valueOfByType(StringUtils.getFilenameExtension(file));
                            if (fileType != null) {
                                long currentLine = 0;
                                while ((line = br.readLine()) != null) {
                                    threadQueue.put(new QueueType(fileType, line, ++currentLine, file));
                                }
                            } else {
                                Order order = new Order(file, "Неизвестный тип расширения файла");
                                System.out.println(order.toString());
                            }
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                            log.error(e.getMessage(), e);
                        } catch (IOException e) {
                            log.error(e.getMessage(), e);
                            Order order = new Order(file, "Не удалось прочитать данный файл");
                            System.out.println(order.toString());
                        } finally {
                            countDownLatch.countDown();
                        }
                    });
                } else {
                    countDownLatch.countDown();
                }
            }
            countDownLatch.await();
            parserExecutor.shutdown();
            threadQueue.fillComplete();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.error(e.getMessage(), e);
        }
    }

    public void convertData() {
        ThreadPoolExecutor converterExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(MAX_COUNT_CONVERTER_THREAD);
        IntStream.range(0, MAX_COUNT_CONVERTER_THREAD - 1)
                 .forEach(idx -> converterExecutor.submit(() -> {
                     while (threadQueue.isContinueWork() || !threadQueue.isEmpty()) {
                         try {
                             QueueType queueType = threadQueue.get();
                             if (queueType != null) {
                                 parsers.get(queueType.getType()).convertFromQueue(queueType, validator);
                             }
                         } catch (InterruptedException e) {
                             Thread.currentThread().interrupt();
                             log.error(e.getMessage(), e);
                         }
                     }
                 }));
        converterExecutor.shutdown();
    }
}
