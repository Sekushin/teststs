package com.home.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.home.model.Currency;
import com.home.model.Order;
import com.home.model.QueueType;
import com.home.service.FileParser;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import lombok.extern.slf4j.Slf4j;

import static org.springframework.util.ObjectUtils.isEmpty;

@Slf4j
public class JSONParserImpl implements FileParser {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void convertFromQueue(QueueType queueType, Validator validator) {
        if (!isEmpty(queueType.getData())) {
            try {
                JsonNode jsonNode = objectMapper.readTree(queueType.getData());
                if (!jsonNode.isEmpty()) {
                    List<String> errors = new ArrayList<>();
                    Order order = new Order();
                    try {
                        JsonNode orderId = jsonNode.get("orderId");
                        if (orderId != null) {
                            order.setId(orderId.asLong());
                        }
                    } catch (Exception ex) {
                        errors.add("Неверный формат идентификатора ордера");
                    }
                    try {
                        JsonNode amount = jsonNode.get("amount");
                        if (amount != null) {
                            order.setAmount(amount.asDouble());
                        }
                    } catch (Exception ex) {
                        errors.add("Неверный формат суммы ордера");
                    }
                    try {
                        JsonNode currency = jsonNode.get("currency");
                        if (currency != null) {
                            order.setCurrency(Currency.valueOfByType(currency.asText()));
                        }
                    } catch (Exception ex) {
                        errors.add("Не удалось определить валюту суммы ордера");
                    }
                    try {
                        JsonNode comment = jsonNode.get("comment");
                        if (comment != null) {
                            order.setComment(comment.asText());
                        }
                    } catch (Exception ex) {
                        errors.add("Не удалось определить комментарий по ордеру");
                    }
                    order.setLine(queueType.getLineNumber());
                    order.setFilename(queueType.getFilename());
                    Set<ConstraintViolation<Order>> violations = validator.validate(order);
                    if (!violations.isEmpty()) {
                        errors.addAll(violations.stream()
                                                .map(ConstraintViolation::getMessage)
                                                .collect(Collectors.toList()));
                    }
                    order.setResult(errors.isEmpty() ? "OK" : String.join(", ", errors));
                    System.out.println(order.toString());
                }
            } catch (Exception e) {
                log.error("Not valid line {} in file {} \n{}", queueType.getLineNumber(), queueType.getFilename(), e.getMessage(), e);
            }
        } else {
            log.warn("Empty line {} in file {}", queueType.getLineNumber(), queueType.getFilename());
        }
    }
}
