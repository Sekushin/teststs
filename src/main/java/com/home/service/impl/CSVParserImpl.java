package com.home.service.impl;

import com.home.model.Currency;
import com.home.model.Order;
import com.home.model.QueueType;
import com.home.service.FileParser;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import lombok.extern.slf4j.Slf4j;

import static org.springframework.util.ObjectUtils.isEmpty;

@Slf4j
public class CSVParserImpl implements FileParser {

    @Override
    public void convertFromQueue(QueueType queueType, Validator validator) {
        String[] data = queueType.getData() != null ? queueType.getData().split(",") : null;
        if (data != null) {
            try {
                List<String> errors = new ArrayList<>();
                Long id;
                try {
                    id = data.length > 0 && !isEmpty(data[0]) ? Long.parseLong(data[0]) : null;
                } catch (Exception ex) {
                    errors.add("Неверный формат идентификатора ордера");
                    id = null;
                }
                Double amount;
                try {
                    amount = data.length > 1 && !isEmpty(data[1]) ? Double.parseDouble(data[1]) : null;
                } catch (Exception ex) {
                    errors.add("Неверный формат суммы ордера");
                    amount = null;
                }
                Currency currency;
                try {
                    currency = data.length > 2 ? Currency.valueOfByType(data[2]) : null;
                } catch (Exception ex) {
                    errors.add("Не удалось определить валюту суммы ордера");
                    currency = null;
                }
                Order order = Order.builder()
                                   .id(id)
                                   .amount(amount)
                                   .currency(currency)
                                   .comment(data.length > 3 ? data[3] : null)
                                   .line(queueType.getLineNumber())
                                   .filename(queueType.getFilename())
                                   .build();
                Set<ConstraintViolation<Order>> violations = validator.validate(order);
                if (!violations.isEmpty()) {
                    errors.addAll(violations.stream()
                                            .map(ConstraintViolation::getMessage)
                                            .collect(Collectors.toList()));
                }
                order.setResult(errors.isEmpty() ? "OK" : String.join(", ", errors));
                System.out.println(order.toString());
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        } else {
            log.warn("Empty line {} in file {}", queueType.getLineNumber(), queueType.getFilename());
        }
    }
}
