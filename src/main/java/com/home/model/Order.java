package com.home.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    @NotNull(message = "Идентификатор ордера пустой")
    @Min(value = 0, message = "Идентификатор ордера меньше 0")
    private Long id;
    @NotNull(message = "Сумма ордера пустая")
    @Min(value = 0, message = "Сумма ордера меньше 0")
    private Double amount;
    @NotNull(message = "Валюта суммы ордера пустая")
    private Currency currency;
    @NotBlank(message = "Комментарий по ордеру пустой")
    private String comment;
    private String filename;
    private long line;
    private String result;

    public Order(String file, String error) {
        this.filename = file;
        this.result = error;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\"=" + id +
                ", \"amount\"=" + amount +
                ", \"comment\"='" + comment + '\'' +
                ", \"filename\"='" + filename + '\'' +
                ", \"line\"=" + line +
                ", \"result\"='" + result + '\'' +
                '}';
    }
}
