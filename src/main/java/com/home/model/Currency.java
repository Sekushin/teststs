package com.home.model;

import java.util.Arrays;

public enum Currency {
    USD,
    EUR;

    public static Currency valueOfByType(String type) {
        return Arrays.stream(values())
                     .filter(t -> t.toString().equalsIgnoreCase(type))
                     .findFirst()
                     .orElse(null);
    }
}
