package com.home.model;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class ThreadSafeQueue<T> {

    private final int timeout;
    private final TimeUnit timeUnit;
    private final BlockingQueue<T> queue;
    private Boolean continueWork = Boolean.TRUE;

    public ThreadSafeQueue(int capacity, int timeout, TimeUnit timeUnit) {
        this.timeout = timeout;
        this.timeUnit = timeUnit;
        queue = new ArrayBlockingQueue<>(capacity);
    }

    public void put(T data) throws InterruptedException {
        this.queue.put(data);
    }

    public T get() throws InterruptedException {
        return this.queue.poll(timeout, timeUnit);
    }

    public int size() {
        return this.queue.size();
    }

    public boolean isEmpty() {
        return this.queue.isEmpty();
    }

    public boolean isContinueWork() {
        return continueWork;
    }

    public void fillComplete() {
        continueWork = false;
    }
}
